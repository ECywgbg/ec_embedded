/*
program starts

printing _ _ _ for s and * * * for o
blink s.o.s every 1sec

program ends when power goes off
crtl c to close


*/

#include <stdio.h>
#include <unistd.h>

void main(){

    do
    {
        // waits for 1 sec before printing s.o.s in morse code
        sleep(1);
        printf("_ _ _ ");
        printf("* * * ");
        printf("_ _ _ \n");

        
    } while (1);
    


}