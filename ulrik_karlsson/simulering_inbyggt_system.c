#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// this function will loop doing nothing for given number of  milli secounds
void delay( int milli_secounds){
  // Storing current time to use as reference
  clock_t start_time = clock();

  // Looping till clock catches up with start time + given number of milli secounds
  while (clock() < start_time + milli_secounds);
}

int main(){
  // this is the instructions for the LED 1=on 0=off
  char instructions[] = "111011101110101010";

  while(1){
    for (int i = 0; i < 18; i++){
      system("cls");
      printf("%c\n", instructions[i]);
      delay(500);
    }
  }
  return 0;
}
